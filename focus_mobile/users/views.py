from django.shortcuts import (
    render,
    redirect
)

from django.http import HttpResponseForbidden

from django.contrib.auth.models import User
from django.contrib.auth import (
    login,
    authenticate
)

from .forms import UserLogInForm
from employees.models import EmployeeDetails

# Create your views here.


def register(request):
    if request.method == 'POST':
        names = request.POST.get('full_name').split(" ")
        first_name = names[0]
        last_name = " ".join(names[1:])
        user_name = request.POST.get('user_name')
        password = request.POST.get('password')
        email = request.POST.get('email')
        user = User.objects.create_user(
            user_name,
            email,
            password,
            first_name=first_name,
            last_name=last_name,
            is_active=False
        )
        return redirect('account-verification', user_id=user.id)

    return render(request, 'users/register.html')


def verification(request, user_id):
    user = User.objects.get(
        pk=user_id
    )
    if request.method == 'POST':
        user = User.objects.get(
            pk=user_id
        )
        user.is_active = True
        user.save()
        login(request, user)
        return redirect('employee-dashboard')

    context = {
        'user': user
    }
    return render(request, 'users/verify_account.html', context)


def login_user(request):
    user_login_form = UserLogInForm(request.POST or None)
    if user_login_form.is_valid():
        username = user_login_form.cleaned_data.get("username")
        password = user_login_form.cleaned_data.get("password")
        user = authenticate(
            username=username,
            password=password
        )
        login(request, user)
        if user.is_superuser:
            return redirect('admin-dashboard')
        else:
            return redirect('employee-dashboard')
    return render(request, "users/login.html", {"form": user_login_form})


def admin_dashboard(request):
    user = request.user
    if not user.is_superuser:
        return HttpResponseForbidden()

    employee_details = EmployeeDetails.objects.all()

    return render(
        request,
        'users/admin_dashboard.html',
        {'employee_details': employee_details}
    )
