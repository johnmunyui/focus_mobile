from django.urls import path
from django.conf import settings
from django.contrib.auth.views import logout

from . import views

urlpatterns = [
    path('register/', views.register),
    path('login/', views.login_user),
    path('admin_dashboard/', views.admin_dashboard, name='admin-dashboard'),
    path('logout/', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}),
    path('verify_account/<int:user_id>/',
         views.verification,
         name='account-verification')
]
