from django import forms

from django.contrib.auth import (
    authenticate,
    get_user_model
)

User = get_user_model()


class UserRegistrationForm(forms.Form):
    full_names = forms.CharField(label='Full names', max_length=50)
    user_name = forms.CharField(label='Username', max_length=15)
    email = forms.EmailField(label='Email')
    password = forms.PasswordInput()
    is_admin = forms.BooleanField(label='Is Admin', required=True)


class UserLogInForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        user = authenticate(
            username=username,
            password=password
        )
        if not user:
            raise forms.ValidationError("This user does not exist")

        if not user.is_active:
            raise forms.ValidationError("This user is not active")

        return super(UserLogInForm, self).clean(*args, **kwargs)
