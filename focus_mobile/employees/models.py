from time import time

from django.db import models
from django.contrib.auth.models import User

# uploaded file name definition


def uploaded_image_name(instance, filename):
    return "uploads/%s_%s" % (str(time()).replace('.', '_'), filename)


# Create your models here.
class EmployeeDetails(models.Model):
    user = models.OneToOneField(User, related_name="employee_details",
                             on_delete=models.CASCADE)
    employee_number = models.CharField(max_length=100)
    tax_id = models.CharField(max_length=100)
    date_hired = models.DateField()
    id_number = models.CharField(max_length=20)
    id_scanned_copy = models.ImageField(upload_to=uploaded_image_name,
                                        default="uploads/no_id.png")
    verifier_phone_number = models.CharField(max_length=100)
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class AdminVerification(models.Model):
    employee_details = models.OneToOneField(EmployeeDetails,
                                         related_name="verified",
                                         on_delete=models.CASCADE
                                         )
    user = models.ForeignKey(User, related_name="verified_by",
                             on_delete=models.CASCADE)
    verified = models.BooleanField()
    comments = models.TextField(null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
