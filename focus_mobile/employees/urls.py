from django.urls import path

from . import views

urlpatterns = [
    path('dashboard/', views.dashboard, name='employee-dashboard'),
    path('add_details/', views.add_details, name='employee-add-details'),
    path('edit_details/', views.edit_details, name='employee-edit-details'),
    path('delete_details/', views.delete_details, name='employee-edit-details'),
    path('verify_data/<int:details_id>/', views.admin_verify_details),
    path('export/', views.export),
]
