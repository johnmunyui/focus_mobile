from django.shortcuts import (
    render,
    redirect
)

import djqscsv

from .models import (
    EmployeeDetails,
    AdminVerification
)
from .forms import (
    EmployeeDetailsForm,
    AdminVerificationForm
)

# Create your views here.


def dashboard(request):
    user = request.user

    try:
        employee_details = EmployeeDetails.objects.get(
            user=user
        )
    except EmployeeDetails.DoesNotExist:
        employee_details = None
        employee_details_verfication = None

    if employee_details:
        try:
            employee_details_verfication = AdminVerification.objects.get(
                employee_details=employee_details
            )
        except AdminVerification.DoesNotExist:
            employee_details_verfication = None

    return render(
        request,
        'employees/dashboard.html',
        {'employee_details': employee_details,
         'verification_details': employee_details_verfication
         }
    )


def add_details(request):
    if request.method == "POST":
        employee_details_form = EmployeeDetailsForm(
            request.POST,
            request.FILES
        )
        employee_details = employee_details_form.save(commit=False)
        employee_details.user = request.user
        employee_details.save()

        return redirect('employee-dashboard')

    return render(
        request,
        'employees/add_details.html',
        {'form': EmployeeDetailsForm()}
    )


def edit_details(request):
    user = request.user

    employee_details = EmployeeDetails.objects.get(
        user=user
    )
    if request.method == "POST":
        employee_details_form = EmployeeDetailsForm(
            request.POST,
            request.FILES,
            instance=employee_details
        )
        employee_details = employee_details_form.save(commit=False)
        employee_details.user = request.user
        employee_details.save()

        return redirect('employee-dashboard')

    employee_details_form = EmployeeDetailsForm(
        instance=employee_details
    )
    return render(
        request,
        'employees/edit_details.html',
        {'form': employee_details_form}
    )


def delete_details(request):
    user = request.user
    if request.method == "POST":
        EmployeeDetails.objects.get(
            user=user
        ).delete()

        return redirect('employee-dashboard')

    return render(
        request,
        'employees/delete_details.html',
    )


def admin_verify_details(request, details_id):
    employee_details = EmployeeDetails.objects.get(
        pk=details_id
    )
    if request.method == "POST":
        admin_verification_form = AdminVerificationForm(
            request.POST
        )
        admin_verification = admin_verification_form.save(commit=False)
        admin_verification.user = request.user
        admin_verification.employee_details = employee_details
        admin_verification.save()
        return redirect('admin-dashboard')

    return render(
        request,
        'employees/admin_verify_data.html',
        {'employee_details': employee_details,
         'form': AdminVerificationForm()}
    )


def export(request):
    employees_details = EmployeeDetails.objects.all()
    return djqscsv.render_to_csv_response(employees_details)
