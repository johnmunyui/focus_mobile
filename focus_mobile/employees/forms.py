from django import forms
from .models import (
    EmployeeDetails,
    AdminVerification
)


class EmployeeDetailsForm(forms.ModelForm):
    class Meta:
        model = EmployeeDetails
        exclude = ['user']


class AdminVerificationForm(forms.ModelForm):
    class Meta:
        model = AdminVerification
        exclude = ['employee_details', 'user']
