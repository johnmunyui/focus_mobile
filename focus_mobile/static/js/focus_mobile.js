 $(document).ready(function(){
    $("#user_create").on("click", function(ev){
       ev.preventDefault() 
        var password =  $("#password").val()
        var passwordConfirm = $("#confirm_password").val()


        if (password !== passwordConfirm){
           $("#error-section").removeClass("hidden") 
            return
        } else {
            $("#createuser").submit()
        }
        
    });

    $(".dateinput").datepicker()
});
